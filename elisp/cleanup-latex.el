
;;; This is run after latex is generated 
(defun cleanup-latex (file)
  (let ((buf (find-file (expand-file-name file))))
	(switch-to-buffer buf)
	(beginning-of-buffer)
	(let ((beg (search-forward "\\begin{HTML}"  nil t))
		  (start (search-backward "\\begin{HTML}" nil t))
		  (end (search-forward "\\end{LATEX}" nil t)))
	  (and beg end (kill-region start end)))
	(save-buffer)))

;;; Author: Venkatesh Choppella <venkatesh.choppell@iiit.ac.in>

;;; Generic command line interface for publishing or exporting

;;; - Publishing a top-level org-project
;;; - Exporting a org file either to html or latex

;;; Usage:

;;; ------
;;; 
;;;     emacs -q --script elisp/cli.el
;;;           <emacs-funs-file>
;;;           path=<path>
;;;           prj-dir=<prj-dir>
;;;           op=<op>
;;;           <name>=<val> ...

;;; 
;;;
;;;      where
;;;        <emacs-funs-file>:  location of emacs-funs.el
;;;      
;;;        <path> :   colon separated list of lisp directories.
;;;                   Default: ""
;;; 
;;;        <prj-dir> : project directory.  Default: default-directory,
;;;                    the directory from which emacs was invoked.
;;;
;;;        <op> : pub | exp
;;;
;;;        <name>=<val> ... : additional fields.  Exact
;;;                           list depends on <op>.


;;;       if <op> = pub: additional fields
;;;          force=<force>:  "0" | anything
;;;          "0" means don't republish unmodified files.
;;;          anything means republish all files


;;;       if <op> = exp:  additional fields
;;;           file=<file>  :  file to be exported
;;; 
;;;           format=<format> :  latex  |   html (default)

(message "entering cli ...")
(message "argv = %s" argv)

;;; read the emacs-funs-file
(let ((emacs-funs-file (elt argv 0)))
  (message "emacs-funs-file = %s" emacs-funs-file)
  (load-file emacs-funs-file))

;;; load-path
;;; ==========
(message "load-path = %s" load-path)

(defvar args-alist (parse-n=v-list (cdr argv)))
(message "arg-alist =")
(print args-alist)

;;; compute path to be prepended to load-path
(defvar path
  (let ((path-str (lookup 'path args-alist nil)))
	(and path-str
		 (split-string path-str ":"))))

(message "path= %s" path)

;;; update load-path
(setq load-path	(append path load-path))
(message "updated load-path = %s" load-path)

(defvar prj-dir
  (lookup 'prj-dir args-alist command-line-default-directory))
(message "-------------------------------------------------------------------")
(message "prj-dir= %s" prj-dir)

(add-to-list 'load-path prj-dir)
(require 'ox-publish)
;;; (require 'ox-bibtex)
(require 'prj-config)
(require 'latex-macros "insert-latex-macros.el")

(setq org-babel-load-languages
	  (cons '(latex-macros . t)
			org-babel-load-languages))

(message "------------------org-babel-load-languages-------------------------------------")
(print org-babel-load-languages)

(defvar op-type (lookup 'op args-alist "exp"))
(message "org-type= %s" op-type)

(pcase op-type
  ("exp" (require 'exp)
   (do-exp prj-dir args-alist))
  (_ (require 'pub)
	 (do-pub prj-dir args-alist)))


   
   



   


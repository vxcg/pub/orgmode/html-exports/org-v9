(require 'emacs-funs)
(autoload 'publish-project
  "pub-prj" "publish project")

;;; src-dir in args-list must be a subdirectory of
;;; prj-dir
(defun do-pub (prj-dir args-alist)
  (let* ((force (lookup 'force args-alist nil))
		 (force (and force (not (string= force "0")))))
	(message "force= %s" force)
	(let* ((prj-src-dir (file-truename (concat-as-dir prj-dir "src"))) ; /prj/src 
		   (src-dir (file-truename (lookup 'src-dir args-alist prj-src-dir))) ; /prj/src/intro
		   (prj-build-dir (concat-as-dir prj-dir "build")) ; /prj/build
		   (prj-docs-dir (concat-as-dir prj-build-dir "docs")) ; /prj/build/docs
		   (prj-code-dir (concat-as-dir prj-build-dir "code"))  ;  /prj/build/code
		   (rel-dir (file-relative-name src-dir prj-src-dir)) ; intro
		   (docs-dir (concat-as-dir prj-docs-dir rel-dir))	; /prj/build/docs/intro
		   (code-dir (concat-as-dir prj-code-dir rel-dir)))   ; /prj/build/code/intro
	  (message "prj-dir = %s" prj-dir)
	  (message "src-dir = %s" src-dir)

	  (message "prj-src-dir = %s" prj-src-dir)
	  (message "prj-build-dir = %s" prj-build-dir)
	  (message "prj-docs-dir = %s" prj-docs-dir)
	  (message "prj-code-dir = %s" prj-code-dir)	  
	  
	  (message "rel-dir = %s" rel-dir)
	  (message "docs-dir = %s" docs-dir)
	  (message "code-dir = %s" code-dir)
	  (shell-command (format "mkdir -p %s; mkdir -p %s"
							 docs-dir code-dir))
	  (if (boundp 'prj-figs) ; usually "figs"
		  (let ((cmd (format "cp -fP %s %s" prj-figs docs-dir)))
			(message "copy cmd = %s" cmd)
			(shell-command cmd)
			))
	  (publish-project src-dir docs-dir code-dir force)
	  )))

(defun publish-project (src-dir docs-dir code-dir force)
  "publish project"
  (interactive)
  ;;; exports  org files html and leaves them in  docs
  (setq org-docs
	  `("org-docs"
		:base-directory ,src-dir
		:base-extension "org"
		:publishing-directory ,docs-dir
		:recursive t
		:exclude ,exclude-rx
		:html-doctype "html5"
		:with-broken-links t
		:publishing-function org-html-publish-to-html
		:headline-levels 6		  ; Just the default for this project.
		:auto-preamble t
;		:auto-postamble 'my-postamble-function
		:auto-sitemap t
		:sitemap-title " "
		))
  (message "org-docs = %s" org-docs)
;;; copies non org (static) files  to docs
  (setq org-static
	  `("org-static"
		:base-directory ,src-dir
		:exclude ,exclude-rx
		:base-extension "png\\|css\\|js\\|txt\\|jpg\\|zip\\|svg\\|pdf"
		:publishing-directory ,docs-dir
		:recursive t
		:publishing-function org-publish-attachment
		))

;;; tangles code out from the *src-dir*
  (setq org-code
	  `("org-code"
		:base-directory ,src-dir
		:base-extension "org"
		:exclude ,exclude-rx
		:publishing-directory ,code-dir
		:recursive t
		;;		:publishing-function tangle-wrapper
		:publishing-function org-babel-tangle-publish
		))
  (message "org-code = %s" org-code)
  (setq prj
      '("prj" :components 
		(
		 "org-docs" 
		 "org-static" 
		 "org-code"
		 )))

  (setq org-publish-project-alist
		(list org-docs org-static org-code prj))

  (message "org-publish-project-alist = %s" org-publish-project-alist)    

  (org-publish-project  
   prj  ; project name
   force ; force
   ))

(provide 'pub)

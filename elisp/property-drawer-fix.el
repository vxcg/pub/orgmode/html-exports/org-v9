;;; redefined.  Original in ox-html.el
;;; changes ox-html.el's  <pre class="example"> to
;;; <pre class="properties">

(defun org-html-property-drawer (_property-drawer contents _info)
  "Transcode a PROPERTY-DRAWER element from Org to HTML into a
<pre> element with properties as HTML attributes.  CONTENTS holds the
contents of the drawer.  INFO is a plist holding contextual
information. "
  (and (org-string-nw-p contents)
	   (let* ((node-properties (cddr _property-drawer))
			  (kvps (mapcar (lambda (node)
							  (let ((pl (cadr node))) ; plist
								(cons (intern (plist-get pl :key))
									  (plist-get pl :value))))
							node-properties)))
		 ;;; check if class is a key, if so, save value
		 (let ((class-val-str (let ((p (assq 'class kvps)))
								(if p
									(format " %s" (cdr p))
								  ""))))
		   (let ((kvps-sans-class (assq-delete-all 'class kvps)))
			 (let ((val ""))
			   (dolist (el kvps-sans-class val)
				 (setq val
					   (format "%s %s=\"%s\" " val (car el) (cdr el))))
			   (format "<pre class=\"properties%s\" %s>\n %s</pre>"
					   class-val-str val contents)))))))
		 

(provide 'property-drawer-fix)

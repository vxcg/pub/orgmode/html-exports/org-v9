;;; invoke from command line like this:
;;; emacs --batch --load hello.el --eval '(hello "there")'
(defun hello (name)
  (message "Hello %s!" name))

(require 'emacs-funs)

(defun do-exp (prj-dir args-alist)
  (let ((org-file
		 (lookup 'file args-alist (concat
								   (file-name-as-directory
									prj-dir)
								   "main.org")))
		(exp-format (lookup 'format args-alist "html")))
	(message "org-file= %s" org-file)
	(message "format = %s" exp-format)
	(let ((exp-fun
		  (pcase exp-format
		   ("latex" 'org-latex-export-as-latex)
		   (_ 'org-html-export-as-html))))
	  (exp org-file exp-format exp-fun))))

(defun format-to-extension (format)
  (pcase format
	("latex" "tex")
	(_ "html")))

(defun exp (file format exp-fun)
  (let* ((fname-ext (format-to-extension format))
		(fname-sans-ext (file-name-sans-extension file))
		(fname (concat fname-sans-ext "." fname-ext)))
	(with-temp-buffer
	  (insert-file-contents file)
	  (with-current-buffer (apply exp-fun '())
		(write-file fname)))))

(provide 'exp)
